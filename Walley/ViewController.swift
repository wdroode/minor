//
//  ViewController.swift
//  Walley
//
//  Created by Shuhui on 19/12/2017.
//  Copyright © 2017 Beanies. All rights reserved.
//

/* Swift imports */
import UIKit

/* Pod imports */
import BiometricAuthentication

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.authenticate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /* Authenticate the user by TouchID */
    func authenticate() {
        let message = "Plaats uw vinger op de thuisknop om verder te gaan"
        
        BioMetricAuthenticator.authenticateWithBioMetrics(reason: message, success: {
            // Switch to 'Mijn transacties' view
            self.performSegue(withIdentifier: "segue1", sender: self)
            
        }) { (error) in
            // Error handling not required in demo
        }
    }
    
}

